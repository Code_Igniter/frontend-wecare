
import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { EndpointBase } from './endpoint-base.service';
import { environment } from '../../../../environments/environment';


@Injectable()
export class DanphecareEndpoint extends EndpointBase {
  
  private readonly _GetAppliedUsrlistUrl: string = "api/danphecare/GetAppliedUsrlist";
  private readonly _applytoIsolationUrl: string = "api/danphecare/ApplyToIsolation";  
  private readonly _reachUsQuicklyUrl: string = "api/danphecare/ReachUsQuickly";  

  constructor( http: HttpClient) {
    super(http);
  }

  GetAppliedUsrlist<T>(): Observable<T>{
    const endpointUrl = `${environment.url}/${this._GetAppliedUsrlistUrl}`;

    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetAppliedUsrlist()));
      }));
  }

  applytoIsolation<T>(userObject: any): Observable<T> {

    return this.http.post<T>(`${environment.url}/${this._applytoIsolationUrl}`, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.applytoIsolation(userObject)));
      }));
  }
  reachUsQuickly<T>(userObject: any): Observable<T> {
    return this.http.post<T>(`${environment.url}/${this._reachUsQuicklyUrl}`, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.reachUsQuickly(userObject)));
      }));
  }
}
