import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { DanphecareEndpoint } from './danphecare-endpoint.service';
import { DanphecareModel, DanpheCareReachUsQuicklyModel } from '../../cms/models/danphecare/danphecare.model';




@Injectable()
export class DanphecareService {
  constructor(private router: Router, private http: HttpClient, private danphecareEndPoint: DanphecareEndpoint) {
  }

  GetAppliedUsrlist(){
    return this.danphecareEndPoint.GetAppliedUsrlist<DanphecareModel[]>();
  }

  applytoIsolation(danphecare: DanphecareModel) {
    return this.danphecareEndPoint.applytoIsolation<DanphecareModel>(danphecare);
  }
  reachUsQuickly(danphecareruq: DanpheCareReachUsQuicklyModel) {
    return this.danphecareEndPoint.reachUsQuickly<DanpheCareReachUsQuicklyModel>(danphecareruq);
  }
}
