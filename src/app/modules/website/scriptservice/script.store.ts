
interface Scripts {
  name: string;
  src: string;
}
export const ScriptStore: Scripts[] = [
  { name: 'jquery', src: './jquery.min.js' },
  { name: 'slick', src: './slick.min.js' },
  { name: 'custom', src: './custom.js' },
  { name: 'moveimg', src: './move-img-effect.js' },
  { name: 'plugins', src: './plugins.js' },
  { name: 'popper', src: './popper.min.js' }
];
