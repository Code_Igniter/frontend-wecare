import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { WebsiteRoutingModule } from "../website/website.routing";

import { WebsiteService } from "./websiteservice/website.service";
import { WebsiteEndpoint } from "./websiteservice/website-endpoint.service";

// import { SharedModule } from "../shared/shared.module";
import { ScriptService } from "./scriptservice/script.service";




@NgModule({
  declarations: [
 
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    WebsiteRoutingModule,
    // SharedModule,

  ],
  providers: [
    WebsiteService, WebsiteEndpoint, ScriptService
  ]
})
export class WebsiteModule {

}
