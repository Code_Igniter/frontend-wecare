

import { Component, OnInit, AfterViewInit } from '@angular/core';
import { DanpheCareDoctor, MetaTag, Package, PackageDetails } from '../../cms/models/danphecare.cms.model';
import { HttpClient } from '@angular/common/http';
import { WebsiteService } from '../websiteservice/website.service';

import { Router } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import { NotificationService } from '../services/notification.service';
import { MetaService } from '@ngx-meta/core';

@Component({
  selector: 'app-package',
  templateUrl: './package.component.html'
})
export class PackageComponent implements OnInit, AfterViewInit {

  public packageDetailsList: Array<PackageDetails> = new Array<PackageDetails>();

  constructor(public http: HttpClient, public websiteService: WebsiteService, public notifyService: NotificationService, public router: Router, private titleService: Title,
    private meta: MetaService) {
    //
  }

  ngOnInit() {
    // this.websiteService.GetPackageDetails().subscribe(res => {
    //   if (res) {
    //     this.packageDetailsList = [];
    //     this.packageDetailsList = Object.assign(this.packageDetailsList, res);
    
   
    //   }
    // },
    //   res => {
    //     //this.notifyService.showError("Info", "No Specialist Found!");
    //   });

  }
  ngAfterViewInit() {
    //this.loadScripts();
  }
  
 


  GoToSpecialistDetails(permalink) {

    this.router.navigate(['/profilepage', permalink]);
  }
}
